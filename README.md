# Detecção e Classificação de Caminhões

Este repositório tem como objetivo manter o código responsável por fazer a classificação de caminhões em imagens.

Inicialmente o problema envolve a detecção dos 5 primeiros modelos:

![picture](/aux_images/quadro_resumo.png)

Para informações detalhadas destes e outros modelos, veja as especificações detalhadas em (docs/DNIT_qfv.pdf)

## Referências:

http://www.guiadotrc.com.br/lei/qresumo.asp

http://www.guiadotrc.com.br/noticias/noticiaID.asp?id=36675

PDF - DNIT (docs/DNIT_qfv.pdf)


## Fases

O desenvolvimento da aplicação abrange 3 fases:

* Captura das imagens
* Marcação das imagens
* Desenvolvimento do detector
* Avaliação dos resultados

### Captura das imagens

Um total de 1541 imagens com diferentes resoluções (4032x2268, 5184x2916, 4000x2250) foram capturadas. Alguns exemplos podem ser vistos nas imagens abaixo.

![picture](/aux_images/samples_trucks.png)

### Marcação das imagens

TODO

### Desenvolvimento do detector

TODO

### Avaliação dos resultados

TODO



